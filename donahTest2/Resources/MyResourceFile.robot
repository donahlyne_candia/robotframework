*** Settings ***
Library  Selenium2Library
Library  ..//ExternalKeyword//MyKeywordLib.py

*** Keywords ***
Open Login Page
    [Arguments]  ${URLSite}  ${Browser}
    Open Browser  ${URLSite}  ${Browser}
    Wait Until Page Contains  SIGN IN TO CONTINUE

Input Username Password
    Input Text  id:username  dcandia-c
    Input Text  id:password  DCw3lcome1

Click Login Button
    Click Button    XPATH://input[@type='submit']

Check Page Title
    Wait Until Element Is Visible  xpath://*[@id="memberSearchContainer"]/div[1]/div[2]/form/div/div[1]/input

Input Search String
    [Arguments]  ${searchStr}

    Input Text  xpath://*[@id="memberSearchContainer"]/div[1]/div[2]/form/div/div[1]/input  ${searchStr}

Click Fnc Button
    [Arguments]  ${btnName}  ${lnkNum}

    Run keyword if  ${btnName} == 'phone'  Click Button  id:button_phone
    Run keyword if  ${btnName} == 'order'  Click Button  id:button_order
    Run keyword if  ${btnName} == 'customer'  Click Button  id:button_customer
    Run keyword if  ${btnName} == 'membership'  Click Button  id:button_membership
    Run keyword if  ${btnName} == 'rma'  Click Button  id:button_rma
    Run keyword if  ${btnName} == 'email'  Click Button  id:button_email
    Run keyword if  ${btnName} == 'name'  Click Button  id:button_name
    Run keyword if  ${btnName} == 'other'  Click LnkSearch Other  ${lnkNum}

Click LnkSearch Other
    [Arguments]  ${lnkNum}
    ${strLnk1}=  set variable  xpath://*[@id="button_other"]/ul/li[
    ${strLnk2}=  set variable  ]/a
    ${strXpath}=  concat_string  ${strLnk1}  ${lnkNum}  ${strLnk2}

    Click Button  id:button_other
    wait until element is visible  xpath://*[@id="button_other"]/ul
    click link  ${strXpath}

Click CID Link
    wait until element is visible  xpath://*[@id="mainSearchRslts"]/tbody/tr
    ${element}=  Get Webelement  xpath://*[@id="mainSearchRslts"]/tbody/tr[1]/td[2]/a
    Click element  ${element}

Click Edit Link
    wait until element is visible  xpath://*[@id="copyText"]
    ${element}=  Get Webelement  xpath://*[@id="mainUIView"]/div[3]/div[1]/div/div/div[3]/a
    Click element  ${element}
    #wait until element is visible  xpath://*[@id="ngdialog11"]/div[2]/div/div[2]/form[1]/div[1]/div/input

Click Credit Link
    [Arguments]  ${lnkName}

    run keyword if  ${lnkName} == 'mc'  click link  xpath://*[@id="mainUIView"]/div[4]/div/div/div/div[1]/div[4]/a
    run keyword if  ${lnkName} == 'sc'  click link  xpath://*[@id="mainUIView"]/div[4]/div/div/div/div[1]/div[5]/a
    run keyword if  ${lnkName} == 'reward'  click link  xpath://*[@id="mainUIView"]/div[4]/div/div/div/div[2]/div[2]/a
    run keyword if  ${lnkName} == 'skip'  click link  xpath://*[@id="mainUIView"]/div[4]/div/div/div/div[2]/div[3]/a
    run keyword if  ${lnkName} == 'crefund'  click link  xpath://*[@id="mainUIView"]/div[4]/div/div/div/div[2]/div[4]/a
    run keyword if  ${lnkName} == 'mmhistory'  click link  xpath://*[@id="mainUIView"]/div[4]/div/div/div/div[2]/div[5]/a

Click Member Quick Access Button
    [Arguments]  ${btnName}

    run keyword if  ${btnName} == 'skip'  click button  xpath://*[@id="mainUIView"]/div[3]/div[2]/div/div/quick-action-buttons/div[1]/button
    run keyword if  ${btnName} == 'hard'  click button  xpath://*[@id="mainUIView"]/div[3]/div[2]/div/div/quick-action-buttons/div[2]/button
    run keyword if  ${btnName} == 'crefund'  click button  xpath://*[@id="mainUIView"]/div[3]/div[2]/div/div/quick-action-buttons/div[3]/button
    run keyword if  ${btnName} == 'convert'  click button  xpath://*[@id="mainUIView"]/div[3]/div[2]/div/div/quick-action-buttons/div[4]/button
    run keyword if  ${btnName} == 'reinstate'  click button  xpath://*[@id="mainUIView"]/div[3]/div[2]/div/div/quick-action-buttons/div[5]/button
    run keyword if  ${btnName} == 'GDPR'  click button  xpath://*[@id="mainUIView"]/div[3]/div[2]/div/div/quick-action-buttons/div[6]/button


Member Info Edit
    [Arguments]  ${fName}  ${lName}  ${email}

    to string  ${fName}
    #log  ${isStrF}

    wait until element is visible  //*[@id="ngdialog1"]/div[2]/div
    run keyword if  ${fName} != ''  input text  xpath://*[@id="ngdialog1"]/div[2]/div/div[2]/form[1]/div[1]/div/input  ${fName}
    run keyword if  ${lName} != ''  input text  xpath://*[@id="ngdialog1"]/div[2]/div/div[2]/form[1]/div[2]/div/input  ${lName}
    run keyword if  ${email} != ''  input text  xpath://*[@id="ngdialog1"]/div[2]/div/div[2]/form[1]/div[3]/div/input  ${email}

    Input Manager Code  8

Input Manager Code
    [Arguments]  ${dialogNo}

    ${dialogName}=  concat string  xpath://*[@id="ndialog  ${dialogNo}  "]/div[2]/div
    Log  ${dialogName}